# Construire votre projet

**/!\** Faire un push de votre projet 2 fois par jour ! à 12h et à 17h. Et bien sûr plus si besoin !

**/!\** Notez au jour le jour ce que vous faites sur votre projet... vos difficultés rencontrées, vos réussites, là où vous en êtes !

## Etape 1 : préparation espace de travail

- Créer un dossier quelques part dans le dossier Web de votre serveur Web
- Ajouter un dépôt Git dans ce dossier ou installez Symfony qui créera lui même ce dépît Git.
- Créer un projet public sur Github ou Gitlab
- Fait un premier `commit` et `push`
- On peut enfin commencer à travailler

## Etape 2 : préparation structure du projet

- Finir d'installer les pré-requis pour votre application. Par exemple avec Symfony : Webpack, Faker...
- Configurer tout ça
- Faire un premier test de connexion à la base, de création d'une entité pour vérifier que tout fonctionne bien
- Installer les dépendances "front" éventuelles pour votre BackOffice (Bootstrap...)

## Etape 3 : création de la maquette HTML, CSS/SASS

- Dans un autre dossier travailler sur votre maquette purement HTML
- Pensez au préalable à créer une maquette graphique complète de votre site (avec Figma ?). Pensez à vous inspirer de ce qu'il se fait chez les autres... pensez pratique, utilisabilité, ergonomie, responsivité... la maquette graphique c'est le Mockup mais avec du graphisme... couleurs, picto.. en gros c'est une image qui est identique à ce que l'on aura à l'écran !
- Ne passez pas trois jours sur votre maquette graphique, vous n'êtes pas graphistes... essayez simplement de ne pas faire quelques chose de moche, et oui mettez vous à la place des utilisateurs, montrez aux autres. Votre site doit donner envie, c'est que le Jury va voir en premier
- Finalisez la création d'un layout HTML qui va représenter la mise en forme de toutes vos pages principales.. .responsif bien sûr. Faites éventuellement les maquettes des pages avec des spécificités...


## Etape 4 : intégration de la maquette dans votre projet

- Il est maintenant tant de modifier le(s) layout(s) de votre projet pour y mettre votre maquette HTML à la place, placer vos vues/blocks au bon endroit...
- Placez vos fichiers CSS/SASS au bon endroit... éventuellement transpilez les avec Webpacks... 
- Si vous avez déjà quelques fonctionnalités JS inhérentes à votre maquette/layout, placez les JS où il faut ou développez ces "petites" fonctionnalutés avec Webpack et Stimulus par exemple... ou Avec Webpack et en JS pure !
- Vérifiez la bonne intégration de tout ça...
- Intégrez les pages statiques (présentation... mentions légales...)
- Cool on peut maintenant passer à l'étape suivante


## Etape 5 : les données

- créez les élements nécessaire à la création de votre base de données
- créez un jeu de données initial pour testez votre projet (faker or not faker ?)
- Faites un premier test d'affichage sur le site de ces données (page des produits, pas des catégories, pages des articles... bref quelques données dynamiques)... finalisez une intégration de vos vue HTML préalablement créée.. appréciez ce moment !
- vérifiez que la base répond bien à votre MCD !


## Etape 5 : les fonctionnalités

- Reportez vous à vos userStory, votre BackLog et la mindMap fournies pour développer progressivement vos fonctionnalités. 
- on commence par le BackOffice ou le FrontOffice ? Tout dépend où ce situe le gros de vos fonctionnalités... 
  - sur un site type AirBnB les fonctionnalités sont principalement sur le Front... 
  - sur un site de type Journalistique les fonctonnalités sont principalement sur le Back, 
  - sur un e-commerce les fonctionnalités sont partagées...

Chaque fonctionnélites doit répondre à un besoin. Si vous n'arriveza pas à faire quelque chose, posez vous les questions : 

- c'est essentiel : oui ? Alors je recherche de l'aide, je regarde ce que j'ai déjà fait, je ne bloque pas des jours sur cette fonctionnalités... eventuellement je passe à autre chose si c'est possible en attendant de trouver une solution
- c'est essentiel : non ? Alors je passe à autre chose et j'indexe là où j'en suis pour finalisez plus tard... 

